<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'Películas';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        

        <div class="row">
            <div class="col-lg-8 text-justify">
                <h2></h2>
                <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>
                <br>
                <h2>Película de la semana</h2>
            </div>
            <?= 
                Html::a(
                    Html::img("@web/imgs/$pelicula",[
                    'class'=>'img-fluid',
                    ])
                    ,["site/verpelicula","id"=>$id]
                    ,['class'=>'']
                    );
            ?>
        </div>

    </div>
</div>
