<?php
use yii\helpers\Html;
?>
<div class="row">
    <div class="col-lg-4">
    <?php
    echo Html::img("@web/imgs/$model->portada",[
        "class"=>"card-img-top"
    ]);
    ?>
    </div>
    <div class="col-lg-8">
        <h2><?= $model->titulo ?><br></h2>
        Duración:<br>
        <?= $model->duracion ?> minutos<br>
        Director:<br>
        <?= $model->director ?><br>
        <?=  
            Html::a('Ver más...', 
            ['site/verpelicula', 'id' => $model->id],
            ['class' => '']
            ); 
        ?><br>
    </div>
</div>
