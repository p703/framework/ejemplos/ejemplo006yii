<?php
use yii\helpers\Html;
?>

  <ul class="list-group list-group-flush">
    <li class="list-group-item">Categoría: <?= $model->categoria ?></li>
    
    <li class="list-group-item">
        <?= Html::a('Ver películas', 
                ['site/ver', 'categoria' => $model->categoria],
                ['class' => 'btn btn-primary']) ?></li>
  </ul>


