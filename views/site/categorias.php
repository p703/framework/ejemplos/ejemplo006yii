<?php
use yii\widgets\ListView;


echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView'=>'_categorias',
    'itemOptions'=>[
        'class'=>'col-lg-4'
        ],
    'options'=>[
        'class'=>'row'
        ],
    'layout'=>"{items}"
]);

