<?php
use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    'dataProvider' => $datos,
    'columns'=>[
        'id',
        'titulo',
        'fecha_estreno',
        [
            'label'=>'Categoria',
            'format'=>'raw',
            'value' => function($data){
                return Html::a($data->categoria,['site/vergrid',"id"=>$data->id]);
            }
        ],
        [
            'label'=>'Portada',
            'format'=>'raw',
            'value' => function($data){
                $url="@web/imgs/$data->portada";
                return Html::img($url,['style'=>'width:200px']);
            }
        ]
        ]
    ]);
