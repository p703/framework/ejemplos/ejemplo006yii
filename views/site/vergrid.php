<?php
use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns'=>[
        'id',
        'titulo',
        [
            'label'=>'portada',//nombre del campo
            'format'=>'raw',
            'value' => function($data){
                $url = '@web/imgs/' . $data->portada;
                return Html::img($url,[
                    'class'=>'img-fluid',
                    'style'=>'width:200px']); 
            }
        ],
            'fecha_estreno',
        ]
    ]);

