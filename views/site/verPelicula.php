<?php
use yii\widgets\DetailView;
use yii\helpers\Html;

echo DetailView::widget([
    "model"=>$pelicula,
    "attributes"=>[
        'id',
        'titulo',
        'descripcion',
        'duracion',
        'fecha_estreno',
        'categoria',
        'director',
        'destacada',
        'pelicula_de_la_semana',
        [
            'label'=>'portada',
            'format'=>'raw',
            'value' => function($data){
                $url = '@web/imgs/' . $data->portada;
                return Html::img($url,[
                    'class'=>'img-fluid',
                    'style'=>'width:200px']); 
            }
        ]
    ]
]);

