<?php
use yii\widgets\ListView;


echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView'=>'_ver',
    'itemOptions'=>[
        'class'=>'col-lg-6'
        ],
    'options'=>[
        'class'=>'row'
        ],
    'layout'=>"{items}"
]);

