<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "peliculas".
 *
 * @property int $id
 * @property string|null $titulo
 * @property string|null $descripcion
 * @property int|null $duracion
 * @property string|null $fecha_estreno
 * @property string|null $categoria
 * @property string|null $portada
 * @property string|null $director
 * @property string|null $destacada
 * @property string|null $pelicula_de_la_semana
 */
class Peliculas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'peliculas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['duracion'], 'integer'],
            [['fecha_estreno'], 'safe'],
            [['titulo', 'destacada'], 'string', 'max' => 30],
            [['descripcion'], 'string', 'max' => 200],
            [['categoria', 'director', 'pelicula_de_la_semana'], 'string', 'max' => 300],
            [['portada'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'titulo' => 'Titulo',
            'descripcion' => 'Descripcion',
            'duracion' => 'Duracion',
            'fecha_estreno' => 'Fecha Estreno',
            'categoria' => 'Categoria',
            'portada' => 'Portada',
            'director' => 'Director',
            'destacada' => 'Destacada',
            'pelicula_de_la_semana' => 'Pelicula De La Semana',
        ];
    }
}
