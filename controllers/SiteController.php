<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Peliculas;
use yii\data\ActiveDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $pelicula=Peliculas::find()
                ->where(['pelicula_de_la_semana'=>1])
                ->one();
        return $this->render('index',[
            'pelicula'=>$pelicula->portada,
            'id'=>$pelicula->id
        ]);
    }

    public function actionListado() {
        //GridView
        $dataProvider=new ActiveDataProvider([
            'query'=> Peliculas::find()
        ]);
        return $this->render("listado",[
            'datos'=>$dataProvider,
        ]);
    }
    
    public function actionCategorias() {
        //listView
        $dataProvider=new ActiveDataProvider([
            'query'=>Peliculas::find()->select("categoria")->distinct()
        ]);
        
        return $this->render("categorias",[
            'dataProvider'=>$dataProvider,
        ]);
    }
    
    public function actionDestacadas() {
        //listView
        $dataProvider=new ActiveDataProvider([
            'query'=>Peliculas::find()->where(["destacada"=>1])
        ]);
        
        return $this->render("destacadas",[
            'dataProvider'=>$dataProvider,
        ]);
    }
    
    /**
     * accion que me muestra la pelicula sobre la que realizo clic
     * en la pagina de inicio
     */
    public function actionVerpelicula($id) {
        $pelicula=Peliculas::findOne($id);
        
        return $this->render("verPelicula",[
            "pelicula"=>$pelicula
        ]);
    }
    
    /**
     * Accion que me muestra las peliculas de una determinada categoria
     * seleccionada en la vista _categoria
     * @param type $categoria categoria seleccionada
     * @return vista donde me va a mostrar las peliculas
     */
    public function actionVer($categoria) {
        $dataProvider=new ActiveDataProvider([
            'query'=> Peliculas::find()->where(["categoria"=>$categoria])
        ]);
        return $this->render("ver",[
            'dataProvider'=>$dataProvider,
        ]);
    }
    
    /**
     * Accion que me muestra las peliculas de una determinada categoria
     * seleccionada en la vista listado
     * @param type $id id de la pelicula seleccionada
     * @return vista vergrid que me muestra las peliculas de la categoria de la pelicula seleccionada
     */
    public function actionVergrid($id) {//me llega un 1
        
        //saco la categoria de la pelicula sobre la que he realizado clic (se envia la id)
        $categoria=Peliculas::findOne($id)->categoria;
        
        $dataProvider=new ActiveDataProvider([
            'query'=>Peliculas::find()->where(["categoria"=>$categoria])
        ]);
        return $this->render("vergrid",[
            "dataProvider"=>$dataProvider
        ]);
    }
    
}